﻿using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;

namespace Bag_With_Friends_Server
{
    internal class Server
    {
        public static Random rand = new Random();

        public static byte modVerMajor = 2;
        public static byte modVerMinor = 0;
        public static byte modVerPatch = 0;

        public static IPEndPoint udpEndPoint = new IPEndPoint(IPAddress.Any, 10801);
        public static UdpClient udpClient = new UdpClient(udpEndPoint);

        public static List<Player> players = new List<Player>(0);
        public static Dictionary<uint, Player> publicLookup = new Dictionary<uint, Player>(0);
        public static Dictionary<uint, Player> privateLookup = new Dictionary<uint, Player>(0);

        public static List<Room> rooms = new List<Room>(0);
        public static Dictionary<long, Room> roomLookup = new Dictionary<long, Room>(0);

        public static uint roomID = 0;
        public static long messageID = 0;

        static void Main(string[] args)
        {
            uint IOC_IN = 0x80000000;
            uint IOC_VENDOR = 0x18000000;
            uint SIO_UDP_CONNRESET = IOC_IN | IOC_VENDOR | 12;
            udpClient.Client.IOControl((int)SIO_UDP_CONNRESET, new byte[] { Convert.ToByte(false) }, null);

            Console.WriteLine("Server started on port " + udpEndPoint.Port);

            Task.Factory.StartNew(PingCallback);

            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    try
                    {
                        var result = await udpClient.ReceiveAsync();
                        Process(result.Buffer, result.RemoteEndPoint);
                    }
                    catch (Exception e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(e);
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                }
            });

            string read;
            do
            {
                read = Console.ReadLine();
            } while (read != "quit");
        }

        public static async Task PingCallback()
        {
            var periodicTimer = new PeriodicTimer(TimeSpan.FromMilliseconds(1000));
            while (await periodicTimer.WaitForNextTickAsync())
            {
                List<Player> toRemove = new List<Player>(0);

                for (int i = 0; i < players.Count; i++)
                {
                    try
                    {
                        while (players[i].messageQueue.Count > 0 && !players[i].messageQueue.First().important)
                        {
                            QueueMessage queueMessage = players[i].messageQueue.Dequeue();
                            udpClient.Send(queueMessage.message, queueMessage.message.Length, players[i].ip);
                        }

                        if (players[i].messageQueue.Count > 0 && players[i].messageQueue.First().important)
                        {
                            QueueMessage queueMessage = players[i].messageQueue.Dequeue();
                            udpClient.Send(queueMessage.message, queueMessage.message.Length, players[i].ip);
                        }

                        if (players[i].responding && DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() - players[i].lastRecievedPing > 10000)
                        {
                            players[i].responding = false;
                            Console.WriteLine(players[i].name + " is not responding! (no pongs error)");

                            if (players[i].room != null)
                            {
                                players[i].room.PlayerNotResponding(players[i]);
                            }
                        }

                        if (!players[i].responding && DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() - players[i].lastRecievedPing > 30000)
                        {
                            Console.WriteLine(players[i].name + " is being removed for not responding! (no pongs error)");
                            toRemove.Add(players[i]);
                        }

                        players[i].lastSentPing = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                        List<byte> message = new List<byte>(1) { (byte)ServerMessage.Ping };
                        udpClient.Send(message.ToArray(), message.Count, players[i].ip);
                    }
                    catch (Exception e)
                    {
                        players[i].responding = false;
                        Console.WriteLine(players[i].name + " is not responding! (send error)");

                        if (players[i].room != null)
                        {
                            players[i].room.PlayerNotResponding(players[i]);
                        }

                        //Console.ForegroundColor = ConsoleColor.Red;
                        //Console.WriteLine(e);
                        //Console.ForegroundColor = ConsoleColor.White;
                    }
                }

                for (int i = 0; i < toRemove.Count; i++)
                {
                    toRemove[i].Yeet();
                }
            }
        }

        public static async void Process(byte[] buffer, IPEndPoint endPoint)
        {
            if (buffer.Length == 0) return;

            ClientMessage message = (ClientMessage)buffer[0];

            if (message.ToString() == buffer[0].ToString()) return;

            if (!noLog.Contains(message))
            {
                Console.WriteLine("Recieved " + message + ", length " + buffer.Length);
            }

            switch (message)
            {
                case ClientMessage.Pong:
                    {
                        if (buffer.Length != 5) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];
                        player.lastRecievedPing = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                        
                        if (!player.responding)
                        {
                            Console.WriteLine(player.name + " is responding!");
                        }

                        player.responding = true;

                        if (player.room != null)
                        {
                            player.room.PlayerPing(player);
                        }
                    }
                    break;

                case ClientMessage.Identify:
                    {
                        if (buffer.Length < 6) return;
                        if (buffer.Length - buffer[1] < 5) return;

                        string playerName = Encoding.UTF8.GetString(buffer, 2, buffer[1]);

                        if (buffer[2 + buffer[1]] < modVerMajor ||
                            buffer[3 + buffer[1]] < modVerMinor ||
                            buffer[4 + buffer[1]] < modVerPatch)
                        {
                            List<byte> notUpToDate = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes("Get the latest version of BWF from bwf.givo.xyz").Length };
                            notUpToDate.AddRange(Encoding.UTF8.GetBytes("Get the latest version of BWF from bwf.givo.xyz"));
                            udpClient.Send(notUpToDate.ToArray(), notUpToDate.Count, endPoint);
                            return;
                        }

                        Console.WriteLine("Player connected: " + playerName);

                        uint privateId = 0;
                        uint publicId = 0;

                        do
                        {
                            privateId = BitConverter.ToUInt32(RandomNumberGenerator.GetBytes(4));
                        } while (privateLookup.ContainsKey(privateId) || privateId == 0);

                        do
                        {
                            publicId = BitConverter.ToUInt32(RandomNumberGenerator.GetBytes(4));
                        } while (publicLookup.ContainsKey(publicId) || publicId == 0);

                        Player newPlayer = new Player(playerName, publicId, privateId, endPoint);

                        List<byte> identify = new List<byte>(1) { (byte)ServerMessage.Identify };
                        long id = messageID++;
                        identify.AddRange(BitConverter.GetBytes(id));
                        identify.AddRange(BitConverter.GetBytes(privateId));
                        identify.AddRange(BitConverter.GetBytes(publicId));
                        newPlayer.SendImportantMessage(identify.ToArray(), id);

                        List<byte> youConnected = new List<byte>(3) { (byte)ServerMessage.Info, 0, (byte)Encoding.UTF8.GetBytes("You connected as " + newPlayer.name).Length };
                        youConnected.AddRange(Encoding.UTF8.GetBytes("You connected as " + newPlayer.name));
                        udpClient.Send(youConnected.ToArray(), youConnected.Count, endPoint);
                    }
                    break;

                case ClientMessage.Recieved:
                    {
                        if (buffer.Length != 13) return;

                        long recievedId = BitConverter.ToInt64(buffer, 1);
                        uint playerId = BitConverter.ToUInt32(buffer, 9);

                        if (privateLookup.ContainsKey(playerId))
                        {
                            privateLookup[playerId].RemoveImportantMessage(recievedId);
                        }
                    }
                    break;

                case ClientMessage.MakeRoom:
                    {
                        if (buffer.Length < 6) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player host = privateLookup[BitConverter.ToUInt32(buffer, 1)];

                        if (host.room != null)
                        {
                            List<byte> alreadyInRoom = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes("Can't make a room while in one!").Length };
                            alreadyInRoom.AddRange(Encoding.UTF8.GetBytes("Can't make a room while in one!"));
                            udpClient.Send(alreadyInRoom.ToArray(), alreadyInRoom.Count, endPoint);
                            return;
                        }

                        if (buffer[5] == 0)
                        {
                            List<byte> invalidRoomName = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes("Invalid room name!").Length };
                            invalidRoomName.AddRange(Encoding.UTF8.GetBytes("Invalid room name!"));
                            udpClient.Send(invalidRoomName.ToArray(), invalidRoomName.Count, endPoint);
                            return;
                        }

                        if (buffer.Length - buffer[5] < 7) return;

                        string roomName = Encoding.UTF8.GetString(buffer, 6, buffer[5]);
                        string roomPass = "";

                        if (buffer[6 + buffer[5]] != 0)
                        {
                            roomPass = Encoding.UTF8.GetString(buffer, 7 + buffer[5], buffer[6 + buffer[5]]);
                        }

                        Room newRoom = new Room(roomID++, roomName, roomPass, host);
                        Console.WriteLine("Player: " + host.name + " made room \"" + roomName + "\" with password \"" + roomPass + "\"");
                    }
                    break;

                case ClientMessage.GetRooms:
                    {
                        if (buffer.Length != 5) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];

                        for (int i = 0; i < rooms.Count; i++)
                        {
                            rooms[i].SendListingToPlayer(player);
                        }
                    }
                    break;

                case ClientMessage.UpdateRoom:
                    {
                        if (buffer.Length < 6) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];

                        if (player.room == null)
                        {
                            List<byte> alreadyInRoom = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes("Can't update a room while not in one!").Length };
                            alreadyInRoom.AddRange(Encoding.UTF8.GetBytes("Can't update a room while not in one!"));
                            udpClient.Send(alreadyInRoom.ToArray(), alreadyInRoom.Count, endPoint);
                            return;
                        }

                        if (buffer[5] == 0)
                        {
                            List<byte> invalidRoomName = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes("Invalid room name!").Length };
                            invalidRoomName.AddRange(Encoding.UTF8.GetBytes("Invalid room name!"));
                            udpClient.Send(invalidRoomName.ToArray(), invalidRoomName.Count, endPoint);
                            return;
                        }

                        if (buffer.Length - buffer[5] < 7) return;

                        string roomName = Encoding.UTF8.GetString(buffer, 6, buffer[5]);
                        string roomPass = Encoding.UTF8.GetString(buffer, 7 + buffer[5], buffer[6 + buffer[5]]);

                        player.room.UpdateRoom(player, roomName, roomPass);
                    }
                    break;

                case ClientMessage.SwitchHost:
                    {
                        if (buffer.Length != 9) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1)) || !publicLookup.ContainsKey(BitConverter.ToUInt32(buffer, 5))) return;

                        Player currentHost = privateLookup[BitConverter.ToUInt32(buffer, 1)];
                        Player newHost = publicLookup[BitConverter.ToUInt32(buffer, 5)];

                        if (currentHost.room != null)
                        {
                            currentHost.room.SwitchHost(currentHost, newHost);
                        }
                    }
                    break;

                case ClientMessage.JoinRoom:
                    {
                        if (buffer.Length < 10) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1)) || !roomLookup.ContainsKey(BitConverter.ToUInt32(buffer, 5))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];
                        Room room = roomLookup[BitConverter.ToUInt32(buffer, 5)];

                        if (player.room != null)
                        {
                            List<byte> alreadyInRoom = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes("Can't join a room while in one!").Length };
                            alreadyInRoom.AddRange(Encoding.UTF8.GetBytes("Can't join a room while in one!"));
                            udpClient.Send(alreadyInRoom.ToArray(), alreadyInRoom.Count, endPoint);

                            return;
                        }

                        if (buffer.Length - buffer[9] < 10) return;

                        string password = Encoding.UTF8.GetString(buffer, 10, buffer[9]);

                        room.AddPlayer(player, password);
                    }
                    break;

                case ClientMessage.LeaveRoom:
                    {
                        if (buffer.Length != 5) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];

                        if (player.room == null)
                        {
                            List<byte> inRoomMessageb = new List<byte>(1) { (byte)ServerMessage.InRoom };
                            long idb = messageID++;
                            inRoomMessageb.AddRange(BitConverter.GetBytes(idb));
                            inRoomMessageb.AddRange(BitConverter.GetBytes(false));
                            player.SendImportantMessage(inRoomMessageb.ToArray(), idb);

                            List<byte> yeetMessageb = new List<byte>(1) { (byte)ServerMessage.Yeet };
                            long id2b = messageID++;
                            yeetMessageb.AddRange(BitConverter.GetBytes(id2b));
                            player.SendImportantMessage(yeetMessageb.ToArray(), id2b);

                            return;
                        }

                        List<byte> leftRoom = new List<byte>(3) { (byte)ServerMessage.Info, 0, (byte)Encoding.UTF8.GetBytes("Left room" + player.room.name).Length };
                        leftRoom.AddRange(Encoding.UTF8.GetBytes("Left room" + player.room.name));
                        udpClient.Send(leftRoom.ToArray(), leftRoom.Count, endPoint);

                        List<byte> inRoomMessage = new List<byte>(1) { (byte)ServerMessage.InRoom };
                        long id = messageID++;
                        inRoomMessage.AddRange(BitConverter.GetBytes(id));
                        inRoomMessage.AddRange(BitConverter.GetBytes(false));
                        player.SendImportantMessage(inRoomMessage.ToArray(), id);

                        List<byte> yeetMessage = new List<byte>(1) { (byte)ServerMessage.Yeet };
                        long id2 = messageID++;
                        yeetMessage.AddRange(BitConverter.GetBytes(id2));
                        player.SendImportantMessage(yeetMessage.ToArray(), id2);

                        player.room.RemovePlayer(player);
                        player.room = null;
                    }
                    break;

                case ClientMessage.UpdateName:
                    {
                        if (buffer.Length < 6) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];

                        if (buffer[5] == 0)
                        {
                            List<byte> invalidName = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes("Invalid name!").Length };
                            invalidName.AddRange(Encoding.UTF8.GetBytes("Invalid name!"));
                            udpClient.Send(invalidName.ToArray(), invalidName.Count, endPoint);
                            return;
                        }

                        if (buffer.Length - buffer[5] < 6) return;

                        string oldName = player.name;
                        string newName = Encoding.UTF8.GetString(buffer, 6, buffer[5]);
                        player.name = newName;

                        if (player.room != null)
                        {
                            player.room.PlayerUpdateName(player, oldName);
                        }
                    }
                    break;

                case ClientMessage.UpdateColor:
                    {
                        if (buffer.Length != 9) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];

                        player.color[0] = buffer[5];
                        player.color[1] = buffer[6];
                        player.color[2] = buffer[7];
                        player.color[3] = buffer[8];

                        if (player.room != null)
                        {
                            player.room.PlayerUpdateColor(player);
                        }
                    }
                    break;

                case ClientMessage.UpdateScene:
                    {
                        if (buffer.Length != 6) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];    

                        player.scene = buffer[5];

                        if (player.room != null)
                        {
                            player.room.PlayerUpdateScene(player, buffer[5]);
                        }
                    }
                    break;

                case ClientMessage.UpdatePosition:
                    {
                        if (buffer.Length != 181) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];

                        if (player.room != null)
                        {
                            player.room.PlayerUpdatePosition(player, buffer);
                        }
                    }
                    break;

                case ClientMessage.Summit:
                    {
                        if (buffer.Length != 6) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];

                        if (player.room != null)
                        {
                            player.room.PlayerSummit(player, buffer[5]);
                        }
                    }
                    break;

                case ClientMessage.Yeet:
                    {
                        if (buffer.Length != 5) return;
                        if (!privateLookup.ContainsKey(BitConverter.ToUInt32(buffer, 1))) return;

                        Player player = privateLookup[BitConverter.ToUInt32(buffer, 1)];
                        player.Yeet();
                    }
                    break;

                default:
                    Console.WriteLine(message.ToString() + " IS NOT IMPLEMENTED");
                    break;
            }

        }

        public static void PrintMessage(byte[] message)
        {
            string msg = "";
            foreach (byte b in message)
            {
                msg += b + " ";
            }
            Console.WriteLine(msg);
        }

        public static List<ClientMessage> noLog = new List<ClientMessage>()
    {
        ClientMessage.Pong,
        ClientMessage.Recieved,
        ClientMessage.UpdateColor,
        ClientMessage.UpdatePosition,
        ClientMessage.TextChat,
        ClientMessage.VoiceChat
    };
    }

    public struct QueueMessage
    {
        public bool important;
        public long id;
        public byte[] message;
    }

    public enum ClientMessage
    {
        Pong = 0,
        Identify = 1,
        Recovery = 2,
        Recieved = 3,
        MakeRoom = 6,
        GetRooms = 7,
        UpdateRoom = 8,
        SwitchHost = 9,
        JoinRoom = 10,
        LeaveRoom = 11,
        KickPlayer = 15,
        BanPlayer = 16,
        UnbanPlayer = 17,
        UpdateName = 21,
        UpdateColor = 22,
        UpdateScene = 23,
        UpdatePosition = 24,
        TextChat = 28,
        VoiceChat = 29,
        Summit = 31,
        Yeet = 32
    }

    public enum ServerMessage
    {
        Ping = 0,
        Identify = 1,
        Info = 4,
        RoomList = 7,
        RoomUpdate = 8,
        HostUpdate = 9,
        AddPlayer = 10,
        RemovePlayer = 11,
        InRoom = 12,
        PlayerPing = 20,
        PlayerName = 21,
        PlayerColor = 22,
        PlayerScene = 23,
        PlayerPosition = 24,
        TextChat = 28,
        VoiceChat = 29,
        Summit = 31,
        Yeet = 32
    }
}