﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Bag_With_Friends_Server
{
    public class Room
    {
        public uint id;
        public string name;
        public string password;
        public Player host;
        public List<Player> players;
        public List<string> bans;

        public Room(uint id, string name, string password, Player host)
        {
            this.id = id;
            this.name = name;
            this.password = password;
            this.host = host;
            this.players = new List<Player>(0);
            this.bans = new List<string>(0);

            AddPlayer(host, this.password);
            SwitchHost(host, host);

            Server.rooms.Add(this);
            Server.roomLookup.Add(this.id, this);
        }

        public void SendListingToPlayer(Player player)
        {
            List<byte> roomListing = new List<byte>(1) { (byte)ServerMessage.RoomList };
            roomListing.AddRange(BitConverter.GetBytes(id));
            roomListing.AddRange(BitConverter.GetBytes(players.Count));
            roomListing.AddRange(BitConverter.GetBytes(password != ""));
            roomListing.Add((byte)Encoding.UTF8.GetBytes(name).Length);
            roomListing.AddRange(Encoding.UTF8.GetBytes(name));
            roomListing.Add((byte)Encoding.UTF8.GetBytes(host.name).Length);
            roomListing.AddRange(Encoding.UTF8.GetBytes(host.name));
            player.SendMessage(roomListing.ToArray(), false);
        }

        public void UpdateRoom(Player player, string name, string password)
        {
            if (player != this.host) return;
            Console.WriteLine("Player: " + host.name + " updated room " + this.name + " with password " + this.password + " to " + name + " with password " + password);

            this.name = name;
            this.password = password;

            List<byte> roomUpdateMessage = new List<byte>(1) { (byte)ServerMessage.RoomUpdate };
            long id = Server.messageID++;
            roomUpdateMessage.AddRange(BitConverter.GetBytes(id));
            roomUpdateMessage.Add((byte)Encoding.UTF8.GetBytes(name).Length);
            roomUpdateMessage.AddRange(Encoding.UTF8.GetBytes(name));
            roomUpdateMessage.Add((byte)Encoding.UTF8.GetBytes(this.password).Length);
            roomUpdateMessage.AddRange(Encoding.UTF8.GetBytes(this.password));
            roomUpdateMessage.AddRange(BitConverter.GetBytes(this.id));
            
            for (int i = 0; i < players.Count; i++)
            {
                players[i].SendImportantMessage(roomUpdateMessage.ToArray(), id);
            }
        }

        public void AddPlayer(Player player, string password)
        {
            if (bans.Contains(player.ip.Address.ToString()))
            {
                List<byte> joinError = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes("You are banned from this room").Length };
                joinError.AddRange(Encoding.UTF8.GetBytes("You are banned from this room"));
                player.SendMessage(joinError.ToArray(), false);

                return;
            }

            if (password != this.password)
            {
                List<byte> joinError = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes("Incorrect password").Length };
                joinError.AddRange(Encoding.UTF8.GetBytes("Incorrect password"));
                player.SendMessage(joinError.ToArray(), false);

                return;
            }

            Console.WriteLine("Player: " + player.name + " joined room " + this.name + " id: " + this.id);

            players.Add(player);
            player.room = this;

            List<byte> newPlayer = new List<byte>(1) { (byte)ServerMessage.AddPlayer };
            long id5 = Server.messageID++;
            newPlayer.AddRange(BitConverter.GetBytes(id5));
            newPlayer.AddRange(BitConverter.GetBytes(player.publicId));
            newPlayer.Add(player.scene);
            newPlayer.AddRange(BitConverter.GetBytes(player == host));
            newPlayer.AddRange(player.color);
            newPlayer.Add((byte)Encoding.UTF8.GetBytes(player.name).Length);
            newPlayer.AddRange(Encoding.UTF8.GetBytes(player.name));

            List<byte> newPlayerMessage = new List<byte>(3) { (byte)ServerMessage.Info, 0, (byte)Encoding.UTF8.GetBytes(player.name + " joined").Length };
            newPlayerMessage.AddRange(Encoding.UTF8.GetBytes(player.name + " joined"));

            for (int i = 0; i < players.Count; i++)
            {
                List<byte> playerJoined = new List<byte>(1) { (byte)ServerMessage.AddPlayer };
                long id6 = Server.messageID++;
                playerJoined.AddRange(BitConverter.GetBytes(id6));
                playerJoined.AddRange(BitConverter.GetBytes(players[i].publicId));
                playerJoined.Add(players[i].scene);
                playerJoined.AddRange(BitConverter.GetBytes(players[i] == host));
                playerJoined.AddRange(players[i].color);
                playerJoined.Add((byte)Encoding.UTF8.GetBytes(players[i].name).Length);
                playerJoined.AddRange(Encoding.UTF8.GetBytes(players[i].name));
                player.SendImportantMessage(playerJoined.ToArray(), id6);

                if (players[i] != player)
                {
                    players[i].SendImportantMessage(newPlayer.ToArray(), id5);
                    players[i].SendMessage(newPlayerMessage.ToArray(), false);
                }
            }

            List<byte> youJoined = new List<byte>(3) { (byte)ServerMessage.Info, 0, (byte)Encoding.UTF8.GetBytes("joined room " + name).Length };
            youJoined.AddRange(Encoding.UTF8.GetBytes("joined room " + name));
            player.SendMessage(youJoined.ToArray(), false);

            List<byte> inRoomMessage = new List<byte>(1) { (byte)ServerMessage.InRoom };
            long id = Server.messageID++;
            inRoomMessage.AddRange(BitConverter.GetBytes(id));
            inRoomMessage.AddRange(BitConverter.GetBytes(true));
            player.SendImportantMessage(inRoomMessage.ToArray(), id);

            List<byte> hostUpdateMessage = new List<byte>(1) { (byte)ServerMessage.HostUpdate };
            long id2 = Server.messageID++;
            hostUpdateMessage.AddRange(BitConverter.GetBytes(id2));
            hostUpdateMessage.AddRange(BitConverter.GetBytes(host.publicId));
            hostUpdateMessage.AddRange(BitConverter.GetBytes(host.publicId));
            player.SendImportantMessage(hostUpdateMessage.ToArray(), id2);

            List<byte> roomUpdateMessage = new List<byte>(1) { (byte)ServerMessage.RoomUpdate };
            long id3 = Server.messageID++;
            roomUpdateMessage.AddRange(BitConverter.GetBytes(id3));
            roomUpdateMessage.Add((byte)Encoding.UTF8.GetBytes(name).Length);
            roomUpdateMessage.AddRange(Encoding.UTF8.GetBytes(name));
            roomUpdateMessage.Add((byte)Encoding.UTF8.GetBytes(this.password).Length);
            roomUpdateMessage.AddRange(Encoding.UTF8.GetBytes(this.password));
            roomUpdateMessage.AddRange(BitConverter.GetBytes(this.id));
            player.SendImportantMessage(roomUpdateMessage.ToArray(), id3);
        }

        public void RemovePlayer(Player player)
        {
            players.Remove(player);

            Console.WriteLine("Player: " + player.name + " left room " + this.name + " id: " + this.id);

            if (players.Count == 0)
            {
                Server.rooms.Remove(this);
                Server.roomLookup.Remove(this.id);

                Console.WriteLine("room " + name + ", id: " + this.id + ", deleted because empty");
                return;
            }

            if (host == player)
            {
                SwitchHost(host, players[0]);
            }

            List<byte> removePlayer = new List<byte>(1) { (byte)ServerMessage.RemovePlayer };
            long id = Server.messageID++;
            removePlayer.AddRange(BitConverter.GetBytes(id));
            removePlayer.AddRange(BitConverter.GetBytes(player.publicId));

            List<byte> removePlayerMessage = new List<byte>(3) { (byte)ServerMessage.Info, 0, (byte)Encoding.UTF8.GetBytes(player.name + " left").Length };
            removePlayerMessage.AddRange(Encoding.UTF8.GetBytes(player.name + " left"));

            for (int i = 0; i < players.Count; i++)
            {
                players[i].SendImportantMessage(removePlayer.ToArray(), id);
                players[i].SendMessage(removePlayerMessage.ToArray(), false);
            }
        }

        public void SwitchHost(Player currentHost, Player newHost)
        {
            if (currentHost == this.host && this.players.Contains(newHost))
            {
                this.host = newHost;

                List<byte> hostMessage = new List<byte>(3) { (byte)ServerMessage.Info, 0, (byte)Encoding.UTF8.GetBytes(newHost.name + " is now host").Length };
                hostMessage.AddRange(Encoding.UTF8.GetBytes(newHost.name + " is now host"));

                List<byte> hostUpdateMessage = new List<byte>(1) { (byte)ServerMessage.HostUpdate };
                long id = Server.messageID++;
                hostUpdateMessage.AddRange(BitConverter.GetBytes(id));
                hostUpdateMessage.AddRange(BitConverter.GetBytes(currentHost.publicId));
                hostUpdateMessage.AddRange(BitConverter.GetBytes(newHost.publicId));

                for (int i = 0; i < players.Count; i++)
                {
                    players[i].SendImportantMessage(hostUpdateMessage.ToArray(), id);
                    players[i].SendMessage(hostMessage.ToArray(), false);
                }
            }
        }

        public void PlayerPing(Player player)
        {
            long ping = player.lastRecievedPing - player.lastSentPing;

            List<byte> updateMessage = new List<byte>(1) { (byte)ServerMessage.PlayerPing };
            updateMessage.AddRange(BitConverter.GetBytes(player.publicId));
            updateMessage.AddRange(BitConverter.GetBytes(Convert.ToUInt32(ping)));

            for (int i = 0; i < players.Count; i++)
            {
                players[i].SendMessage(updateMessage.ToArray(), true);
            }
        }

        public void PlayerUpdateName(Player player, string oldName)
        {
            List<byte> renamed = new List<byte>(3) { (byte)ServerMessage.Info, 0, (byte)Encoding.UTF8.GetBytes(oldName + " is now " + player.name).Length };
            renamed.AddRange(Encoding.UTF8.GetBytes(oldName + " is now " + player.name));

            List<byte> updateMessage = new List<byte>(1) { (byte)ServerMessage.PlayerName };
            long id = Server.messageID++;
            updateMessage.AddRange(BitConverter.GetBytes(id));
            updateMessage.AddRange(BitConverter.GetBytes(player.publicId));
            updateMessage.Add((byte)Encoding.UTF8.GetBytes(player.name).Length);
            updateMessage.AddRange(Encoding.UTF8.GetBytes(player.name));

            for (int i = 0; i < players.Count; i++)
            {
                players[i].SendImportantMessage(updateMessage.ToArray(), id);
                players[i].SendMessage(renamed.ToArray(), false);
            }
        }

        public void PlayerUpdateColor(Player player)
        {
            List<byte> updateMessage = new List<byte>(1) { (byte)ServerMessage.PlayerColor };
            updateMessage.AddRange(BitConverter.GetBytes(player.publicId));
            updateMessage.AddRange(player.color);

            for (int i = 0; i < players.Count; i++)
            {
                players[i].SendMessage(updateMessage.ToArray(), true);
            }
        }

        public void PlayerUpdateScene(Player player, byte scene)
        {
            List<byte> updateMessage = new List<byte>(1) { (byte)ServerMessage.PlayerScene };
            long id = Server.messageID++;
            updateMessage.AddRange(BitConverter.GetBytes(id));
            updateMessage.AddRange(BitConverter.GetBytes(player.publicId));
            updateMessage.Add(scene);
            
            for (int i = 0; i < players.Count; i++)
            {
                players[i].SendImportantMessage(updateMessage.ToArray(), id);
            }
        }

        public void PlayerUpdatePosition(Player player, byte[] updateMessage)
        {
            updateMessage[0] = (byte)ServerMessage.PlayerPosition;

            byte[] privateIdBytes = BitConverter.GetBytes(player.publicId);
            updateMessage[1] = privateIdBytes[0];
            updateMessage[2] = privateIdBytes[1];
            updateMessage[3] = privateIdBytes[2];
            updateMessage[4] = privateIdBytes[3];

            for (int i = 0; i < players.Count; i++)
            {
                players[i].SendMessage(updateMessage.ToArray(), true);
            }
        }

        public void PlayerSummit(Player player, byte scene)
        {
            List<byte> summitMessage = new List<byte>(1) { (byte)ServerMessage.Summit };
            summitMessage.AddRange(BitConverter.GetBytes(player.publicId));
            summitMessage.Add(scene);

            for (int i = 0; i < players.Count; i++)
            {
                players[i].SendMessage(summitMessage.ToArray(), false);
            }
        }

        public void PlayerNotResponding(Player player)
        {
            List<byte> respondingMessage = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes(player.name + " is not responding!").Length };
            respondingMessage.AddRange(Encoding.UTF8.GetBytes(player.name + " is not responding!"));

            for (int i = 0; i < players.Count; i++)
            {
                players[i].SendMessage(respondingMessage.ToArray(), true);
            }
        }

        public void PlayerRemovedNotResponding(Player player)
        {
            List<byte> respondingMessage = new List<byte>(3) { (byte)ServerMessage.Info, 1, (byte)Encoding.UTF8.GetBytes(player.name + " is being removed for not responding!").Length };
            respondingMessage.AddRange(Encoding.UTF8.GetBytes(player.name + " is being removed for not responding!"));

            for (int i = 0; i < players.Count; i++)
            {
                players[i].SendMessage(respondingMessage.ToArray(), true);
            }

            RemovePlayer(player);
        }
    }
}
