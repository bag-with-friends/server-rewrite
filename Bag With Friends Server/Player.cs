﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Bag_With_Friends_Server
{
    public class Player
    {
        public IPEndPoint ip;
        public string name;
        public uint publicId;
        public uint privateId;
        public byte scene = 0;
        public byte[] color;
        public Room? room;
        public bool responding;
        public long lastSentPing;
        public long lastRecievedPing;
        public Queue<QueueMessage> messageQueue = new Queue<QueueMessage>(0);

        public Player(string name, uint publicId, uint privateId, IPEndPoint ip)
        {
            this.name = name;
            this.publicId = publicId;
            this.privateId = privateId;
            this.room = null;
            this.ip = ip;
            this.lastSentPing = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            this.lastRecievedPing = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            this.responding = true;
            this.color = new byte[4] { 255, 255, 255, 255 };

            Server.players.Add(this);
            Server.privateLookup.Add(privateId, this);
            Server.publicLookup.Add(publicId, this);
        }

        public void SendMessage(byte[] message, bool ignorable)
        {
            if (messageQueue.Count != 0 && !ignorable)
            {
                messageQueue.Enqueue(new QueueMessage { message = message, important = false });
            } else
            {
                try
                {
                    Server.udpClient.Send(message, message.Length, ip);
                } catch (Exception e)
                {
                    responding = false;
                    Console.WriteLine(name + " is not responding! (send error)");

                    if (room != null)
                    {
                        room.PlayerNotResponding(this);
                    }
                }
            }
        }

        public void SendImportantMessage(byte[] message, long id, bool clearQueue = false)
        {
            messageQueue.Enqueue(new QueueMessage { message = message, important = true, id = id });

            if (messageQueue.Count == 1)
            {
                try
                {
                    Server.udpClient.Send(message, message.Length, ip);
                }
                catch (Exception e)
                {
                    responding = false;
                    Console.WriteLine(name + " is not responding! (send error)");

                    if (room != null)
                    {
                        room.PlayerNotResponding(this);
                    }
                }
            }
        }

        public void RemoveImportantMessage(long id)
        {
            if (messageQueue.Count == 0) return;

            if (messageQueue.First().id == id)
            {
                messageQueue.Dequeue();

                while (messageQueue.Count > 0 && !messageQueue.First().important)
                {
                    QueueMessage message = messageQueue.Dequeue();
                    Server.udpClient.Send(message.message, message.message.Length, ip);
                }

                if (messageQueue.Count > 0 && messageQueue.First().important)
                {
                    QueueMessage message = messageQueue.Dequeue();
                    Server.udpClient.Send(message.message, message.message.Length, ip);
                }
            }
        }

        public void Yeet()
        {
            if (room != null)
            {
                if (responding)
                {
                    room.RemovePlayer(this);
                } else
                {
                    room.PlayerRemovedNotResponding(this);
                }
            }

            Server.players.Remove(this);
            Server.publicLookup.Remove(publicId);
            Server.privateLookup.Remove(privateId);
        }
    }
}
